/*
 ============================================================================
 Name        : i2c.h
 Author      : Guillermo Rodriguez, Rodrigo Olmos
 Version     : 1.0
 Description : Header file of the I2C library made for the project
 ============================================================================
 */

#ifndef I2C_H_
#define I2C_H_

//Libraries
#include <stdio.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>

//Declaration
int i2c_open(void);
int i2c_write(int fd, unsigned char dev_addr, unsigned char reg_addr, unsigned char val);
int i2c_read(int fd, unsigned char addr, unsigned char reg, unsigned char *val);

#endif /* I2C_H_ */
